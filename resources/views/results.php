<?php $this->layout('baseLayout') ?>

<div>
    <h1>Results</h1>
    <?php foreach ($properties as $property): ?>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?= $property['property_name'] ?></h5>

                <dl class="row">
                    <dt class="col-sm-3">Location:</dt>
                    <dd class="col-sm-9"><?= $property['location_name']?></dd>

                    <dt class="col-sm-3">Sleeps:</dt>
                    <dd class="col-sm-9"><?= $property['sleeps']?></dd>

                    <dt class="col-sm-3">Beds:</dt>
                    <dd class="col-sm-9"><?= $property['beds']?></dd>

                    <dt class="col-sm-3">Allows Pets:</dt>
                    <dd class="col-sm-9"><?= $property['accepts_pets'] ? 'Yes' : 'No'?></dd>

                    <dt class="col-sm-3">Near Beach:</dt>
                    <dd class="col-sm-9"><?= $property['near_beach'] ? 'Yes' : 'No'?></dd>
                </dl>
            </div>
        </div>
    <br/>
    <?php endforeach; ?>
    <div>
        <?php if(isset($prevPageLink)): ?>
            <a class="btn btn-primary" href="<?= $prevPageLink ?>">Previous Page</a>
        <?php endif; ?>
        <?php if(isset($nextPageLink)): ?>
            <a class="btn btn-primary" href="<?= $nextPageLink ?>">Next Page</a>
        <?php endif; ?>
    </div>
</div>
