<?php $this->layout('baseLayout') ?>

<form method="get" action="/results">
    <div class="row">
        <div class="col">
            <label for="location">Location</label>
            <input type="text" class="form-control" id="location" name="location">
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col">
            <label for="location">Beds (minimum)</label>
            <input type="number" class="form-control" id="location" name="beds-minimum">
        </div>
        <div class="col">
            <label for="location">Sleeps (minimum)</label>
            <input type="number" class="form-control" id="location" name="sleeps-minimum">
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col">
            <label for="location">Availability From</label>
            <input type="date" class="form-control" id="location" name="availability-from">
        </div>
        <div class="col">
            <label for="location">Availability To</label>
            <input type="date" class="form-control" id="location" name="availability-to">
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="true" id="defaultCheck1" name="near-beach">
                <label class="form-check-label" for="defaultCheck1">
                    Near Beach
                </label>
            </div>
        </div>
        <div class="col">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="true" id="defaultCheck1" name="accepts-pets">
                <label class="form-check-label" for="defaultCheck1">
                    Accepts Pets
                </label>
            </div>
        </div>
    </div>
    <br/>

    <button type="submit" class="btn btn-primary">Search</button>
</form>
