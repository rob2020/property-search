<?php
namespace SykesApp\Repositories;

use Doctrine\DBAL\Query\QueryBuilder;
use SykesApp\Queries\SearchQuery;

class PropertiesRepository
{
    /**
     * @var \Doctrine\DBAL\Driver\Connection
     */
    private $dbConnection;

    public function __construct()
    {
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'dbname' => 'sykes_interview',
            'user' => getenv('user'),
            'password' => getenv('password'),
            'host' => getenv('host'),
            'driver' => getenv('driver'),
        );

        $this->dbConnection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
    }

    /**
     * @param \SykesApp\Queries\SearchQuery $searchQuery
     *
     * @return array
     */
    public function getPropertiesPage(SearchQuery $searchQuery): array
    {
        $dbQuery = $this->buildDbQueryFromSearchQuery($searchQuery);

        $totalMatches = $dbQuery->execute()->rowCount();

        $dbQuery
            ->setFirstResult($searchQuery->getPaginationOffset())
            ->setMaxResults($searchQuery->getPageSize());

        $pageItems = $dbQuery->execute()->fetchAll();

        $output = [
            'properties' => $pageItems,
            'pageNumber' => $searchQuery->getPageNumber(),
            'totalItems' => $totalMatches,
            'totalPages' => (int) ceil($totalMatches / $searchQuery->getPageSize()),
        ];

        if (! $searchQuery->isFirstPage()) {
            $output['prevPage'] = $searchQuery->getPrevPageNumber();
        }

        $lastItemInPage = $searchQuery->getPaginationOffset() + $searchQuery->getPageSize();


        if ($lastItemInPage < $totalMatches) {
            $output['nextPage'] = $searchQuery->getNextPageNumber();
        }

        return $output;

    }

    /**
     * @param \SykesApp\Queries\SearchQuery $searchQuery
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function buildDbQueryFromSearchQuery(SearchQuery $searchQuery): QueryBuilder
    {
        $dbQuery = $this->dbConnection->createQueryBuilder();

        $selectedCols = [
            'p.property_name',
            'p.sleeps',
            'p.beds',
            'p.near_beach',
            'p.accepts_pets',
            'l.location_name',
        ];

        $dbQuery
            ->from('properties', 'p')
            ->join('p', 'locations', 'l', 'p._fk_location = l.__pk');

        if ($searchQuery->locationIsSet()) {
            $dbQuery
                ->andWhere('l.location_name LIKE :location')
                ->setParameter('location', '%' . $searchQuery->getLocationFilter() . '%');
        }

        if ($searchQuery->nearBeachIsSet()) {
            $dbQuery
                ->andWhere('p.near_beach = :nearBeach')
                ->setParameter('nearBeach', $searchQuery->getNearBeachFilter() ? 1 : 0);
        }

        if ($searchQuery->acceptsPetsIsSet()) {
            $dbQuery
                ->andWhere('p.accepts_pets = :acceptsPets')
                ->setParameter('acceptsPets', $searchQuery->getAcceptsPetsFilter() ? 1 : 0);
        }

        if ($searchQuery->sleepsMinimumIsSet()) {
            $dbQuery
                ->andWhere('p.sleeps >= :sleepsMinimum')
                ->setParameter('sleepsMinimum', $searchQuery->getSleepsMinimumFilter());
        }

        if ($searchQuery->bedsMinimumIsSet()) {
            $dbQuery
                ->andWhere('p.beds >= :bedsMinimum')
                ->setParameter('bedsMinimum', $searchQuery->getBedsMinimumFilter());
        }

        if ($searchQuery->availabilityIsSet()) {
            $startDate = $searchQuery->getAvailabilityFrom();
            $endDate = $searchQuery->getAvailabilityTo();

            if ($startDate > $endDate) {
                throw new \InvalidArgumentException('start date must be before end date');
            }

            $dbQuery
                ->leftJoin(
                    'p',
                    'bookings',
                    'b',
                    'p.__pk = b._fk_property AND ('
                        . ' (start_date <= :startDate AND end_date >= :startDate)'
                        . ' OR (start_date < :endDate AND end_date >= :endDate )'
                        . ' OR (:startDate <= start_date AND :endDate >= start_date)'
                    . ')'
                )
                // AND b.start_date =< :availableTo AND b.end_date => :startDate
                ->setParameter('endDate', $endDate->format('Ymd'))
                ->setParameter('startDate', $startDate->format('Ymd'));

            $selectedCols[] = 'b.__pk';
            $dbQuery->where('b.__pk IS NULL');
        };

        $dbQuery->select($selectedCols);

        return $dbQuery;
    }
}
