<?php
namespace SykesApp\Controllers;

use DateTime;
use League\Plates\Engine;
use SykesApp\Repositories\PropertiesRepository;
use SykesApp\Queries\SearchQuery;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class SearchController
 */
class SearchController
{
    /**
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     *
     * @return \Slim\Http\Response
     */
    public static function search(Request $request, Response $response)
    {
        $templates = self::getTemplateEngine();

        return $response->withStatus(200)->write($templates->render('search'));
    }

    /**
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     *
     * @return \Slim\Http\Response
     */
    public static function results(Request $request, Response $response)
    {
        $searchQuery = self::buildSearchQuery($request);

        $propertiesRepository = new PropertiesRepository();
        $propertyPage = $propertiesRepository->getPropertiesPage($searchQuery);

        $data = ['properties' => $propertyPage['properties']];

        if (isset($propertyPage['nextPage'])) {
            $nextPageParams = $request->getParams();
            $nextPageParams['page'] = $propertyPage['nextPage'];
            $data['nextPageLink'] = '/results?' . http_build_query($nextPageParams);
        }

        if (isset($propertyPage['prevPage'])) {
            $prevPageParams = $request->getParams();
            $prevPageParams['page'] = $propertyPage['prevPage'];
            $data['prevPageLink'] = '/results?' . http_build_query($prevPageParams);
        }

        $templates = self::getTemplateEngine();

        return $response->withStatus(200)->write($templates->render('results', $data));
    }

    /**
     * @return \League\Plates\Engine
     */
    private static function getTemplateEngine(): Engine
    {
        return new Engine('./resources/views');
    }

    /**
     * @param \Slim\Http\Request $request
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    private static function buildSearchQuery(Request $request): SearchQuery
    {
        $availabilityFrom = DateTime::createFromFormat('Y-m-d', $request->getParam('availability-from'));
        $availabilityTo = DateTime::createFromFormat('Y-m-d', $request->getParam('availability-to'));

        $currentPage = $request->getParam('page') ?: 1;
        $limit = 2;

        $searchQuery = new SearchQuery($currentPage, $limit);
        $params = $request->getParams();

        $searchQuery
            ->setLocationFilter((string)$params['location'] ?: null)
            ->setBedsMinimumFilter((int)$params['beds-minimum'] ?: null)
            ->setSleepsMinimumFilter((int)$params['sleeps-minimum'] ?: null)
            ->setAvailabilityFilter($availabilityFrom ?: null, $availabilityTo ?: null)
            ->setAcceptsPetsFilter(isset($params['accepts-pets']) && $params['accepts-pets'] === 'true' ?: null)
            ->setNearBeachFilter(isset($params['near-beach']) && $params['near-beach'] === 'true' ?: null);
        return $searchQuery;
    }
}
