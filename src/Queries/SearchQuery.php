<?php

namespace SykesApp\Queries;

class SearchQuery
{
    /** @var int */
    private $pageNumber;

    /** @var int */
    private $pageSize;

    /** @var string|null */
    private $locationFilter;

    /** @var bool|null */
    private $nearBeachFilter;

    /** @var bool|null */
    private $acceptsPetsFilter;

    /** @var int|null */
    private $sleepsMinimumFilter;

    /** @var int|null */
    private $bedsMinimumFilter;

    /** @var \DateTimeInterface|null */
    private $availabilityFrom;

    /** @var \DateTimeInterface|null */
    private $availabilityTo;

    /**
     * SearchQuery constructor.
     *
     * @param int $pageNumber
     * @param int $pageSize
     */
    public function __construct(int $pageNumber, int $pageSize)
    {
        $this->setPageNumber($pageNumber);
        $this->setPageSize($pageSize);
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setPageNumber(int $pageNumber): SearchQuery
    {
        $this->pageNumber = $pageNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setPageSize(int $pageSize): SearchQuery
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getNextPageNumber(): int
    {
        return $this->pageNumber + 1;
    }

    /**
     * @return int
     */
    public function getPrevPageNumber(): int
    {
        return $this->pageNumber - 1;
    }

    /**
     * @return bool
     */
    public function isFirstPage(): bool
    {
        return $this->getPageNumber() === 1;
    }

    /**
     * @return int
     */
    public function getPaginationOffset()
    {
        return $this->getPageSize() * ($this->getPageNumber() - 1);
    }

    /**
     * @return bool
     */
    public function locationIsSet()
    {
        return $this->locationFilter !== null;
    }

    /**
     * @return string|null
     */
    public function getLocationFilter(): ?string
    {
        return $this->locationFilter;
    }

    /**
     * @param string|null $locationFilter
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setLocationFilter(?string $locationFilter): SearchQuery
    {
        $this->locationFilter = $locationFilter;

        return $this;
    }

    /**
     * @return bool
     */
    public function nearBeachIsSet()
    {
        return $this->nearBeachFilter !== null;
    }

    /**
     * @return bool|null
     */
    public function getNearBeachFilter(): ?bool
    {
        return $this->nearBeachFilter;
    }

    /**
     * @param bool|null $nearBeachFilter
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setNearBeachFilter(?bool $nearBeachFilter): SearchQuery
    {
        $this->nearBeachFilter = $nearBeachFilter;

        return $this;
    }

    /**
     * @return bool
     */
    public function acceptsPetsIsSet()
    {
        return $this->acceptsPetsFilter !== null;
    }

    /**
     * @return bool|null
     */
    public function getAcceptsPetsFilter(): ?bool
    {
        return $this->acceptsPetsFilter;
    }

    /**
     * @param bool|null $acceptsPetsFilter
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setAcceptsPetsFilter(?bool $acceptsPetsFilter): SearchQuery
    {
        $this->acceptsPetsFilter = $acceptsPetsFilter;

        return $this;
    }

    /**
     * @return bool
     */
    public function sleepsMinimumIsSet(): bool
    {
        return $this->sleepsMinimumFilter !== null;
    }

    /**
     * @return int|null
     */
    public function getSleepsMinimumFilter(): ?int
    {
        return $this->sleepsMinimumFilter;
    }

    /**
     * @param int|null $sleepsMinimumFilter
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setSleepsMinimumFilter(?int $sleepsMinimumFilter): SearchQuery
    {
        $this->sleepsMinimumFilter = $sleepsMinimumFilter;

        return $this;
    }

    /**
     * @return bool
     */
    public function bedsMinimumIsSet(): bool
    {
        return $this->bedsMinimumFilter !== null;
    }

    /**
     * @return int|null
     */
    public function getBedsMinimumFilter(): ?int
    {
        return $this->bedsMinimumFilter;
    }

    /**
     * @param int|null $bedsMinimumFilter
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setBedsMinimumFilter(?int $bedsMinimumFilter): SearchQuery
    {
        $this->bedsMinimumFilter = $bedsMinimumFilter;

        return $this;
    }

    /**
     * @return bool
     */
    public function availabilityIsSet(): bool
    {
        return $this->availabilityFrom !== null
            && $this->availabilityTo !== null;
    }

    /**
     * @param \DateTimeInterface|null $availabilityFrom
     * @param \DateTimeInterface|null $availabilityTo
     *
     * @return \SykesApp\Queries\SearchQuery
     */
    public function setAvailabilityFilter(
        ?\DateTimeInterface $availabilityFrom,
        ?\DateTimeInterface $availabilityTo
    ): SearchQuery {
        $this->availabilityFrom = $availabilityFrom;
        $this->availabilityTo = $availabilityTo;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getAvailabilityFrom(): ?\DateTimeInterface
    {
        return $this->availabilityFrom;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getAvailabilityTo(): ?\DateTimeInterface
    {
        return $this->availabilityTo;
    }
}
