<?php

use Dotenv\Dotenv;

require_once './vendor/autoload.php';

$dotenv = Dotenv::create(__DIR__ . '/../');
$dotenv->load();

// instantiate the App object
$app = new \Slim\App();

// routes
$app->get('/', '\SykesApp\Controllers\SearchController::search');
$app->get('/results', '\SykesApp\Controllers\SearchController::results');


// Run application
$app->run();
