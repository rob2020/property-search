### Sykes Test

#### Instructions
Requirements
- php7.3 or greater
- composer
- mysql (other database will likely work but not tested)

Run `composer install` from the projects root dir.

Run `cp .env-exmaple .env` from the projects root dir.

Add you DB details to the `.env`

Populate you'r database with the sql.

You can just use php built in server to run the application

Run `php -S localhost:8888 -t public public/index.php` from the projects root dir.

Navigate to `localhost:8888` in you'r browser.

#### External Liberties
- Slim - I just used this for the routes and the request and response objects in the "controller", I could of used it's 
container to show some dependency injection but decided against it. It keeps things very simple see `public/index.php`.

- .env - I don't like committing DB credentials and I didn't want to tell you to edit the code to get the db working so
this seemed like a good compromise.

- dbal - I opted for than pdo just because building up the sql where clauses as a string would of being a bit messy 
dbal's oo approach to query building simplified things abit. + I'd use this or a full blown ORM my day to day work.

- league/plates - I wanted some basic templating for the views. It's essential just phps built in templating 
(alternative syntax) with a couple of helpers thrown in.

#### Design decisions
- No error handling
- Minimum validation i.e you can select dates in the past.
- Minimum UI


